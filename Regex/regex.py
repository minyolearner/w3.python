# A RegEx, or Regular Expression, is a sequence of characters that forms a search pattern.

# RegEx can be used to check if a string contains the specified search pattern.

import re

txt = 'Jihyo is very cute and Tyuzu cute too'

# Basic Search
x = re.search("J",txt)

print(x)

if x:
    print('Success')

pattern = 'M'
y = re.search(pattern, txt)

if y:
    print('Success')
else:
    print('Failed')
    
    
# findall()
# The findall() function returns a list containing all matches.
find = re.findall('Ji', txt) 

print(find)
# ['Ji']

# search()

# The search() function searches the string for a match, and returns a Match object if there is a match.

# If there is more than one match, only the first occurrence of the match will be returned:

ser = re.search("\s", txt)

print(ser.start())





# split()
# Return a list
# String to list convert with seprator

sp = re.split('\s',txt)

print(sp)


# You can control the number of occurrences by specifying the maxsplit parameter

mxsp = re.split('\s', txt, 2)

print(mxsp)
#['Jihyo', 'is', 'very cute']





# Replacing Txt sub()

rep1 = re.sub("\s", "27", txt)

print(rep1)

# Control Replacing via count parameter

rep2 = re.sub("/s","27",txt, 2)

print(rep2)


# Match Object

mato = re.search("c", txt)

print(mato)

# Some Match Object method
# Real Pattern use korle value change hbe!!

print(mato.span()) # koto theke koto (14, 15) ... btw index start from zero.. firster ta show korbe
# Print the position (start- and end-position) of the first match occurrence.

print(mato.string)
#  returns the string passed into the function


print(mato.group()) 
# The regular expression looks for any words that starts with an upper case "S"
# https://www.w3schools.com/python/trypython.asp?filename=demo_regex_match_group