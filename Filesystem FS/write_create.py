
# Writing a file
# "x" - Create - will create a file, returns an error if the file exist

# "a" - Append - will create a file if the specified file does not exist

# "w" - Write - will create a file if the specified file does not exist


'''
try:
    openfile = open('dahy.txt','w')
    try:
        openfile.write('Some Content') #will replace old text
    except:
        print('Writing Error')
    finally:
        openfile.close()
except:
    print('opening file error')
    
'''
    
# Append to last
# 

openfile = open('dahy.txt',"a")

openfile.write('Some Content \n') #will replace old text

openfile.close()



