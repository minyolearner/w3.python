
# Reading File Basic

try:
  openfile = open("momo.txt","r")
  try:
    print(openfile.read())
  except:
    print("Something went wrong when writing to the file")
  finally:
    openfile.close() # U need to close 
except:
  print("Something went wrong when opening the file")
  
  

# u can define how many char to be read

openfile = open("momo.txt","r")

print(openfile.read(1)) 

openfile.close()

# Readline


openfile = open("momo.txt","r")

print(openfile.readline()) 
print(openfile.readline()) 

openfile.close()