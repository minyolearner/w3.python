# Note: If you have Python version 3.4 or later, PIP is included by default.

# check if pip package manager installed : pip --version 
# pip 22.2.2 from /usr/lib/python3.11/site-packages/pip (python 3.11)

# installing package: pip install packagename

# removing a package: pip uninstall packagename

# list of package installed on system: pip list