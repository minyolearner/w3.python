#Slicing
txt = 'I Love Jihyo'

print(txt[7:12]) #start after 7  ##Index Also start from 1

## Slice From Start 

print(txt[:12])

## Slice From Position

print(txt[2:12])


### Negative Indexing

print(txt[-6:-1]) # print jihy
print(txt[-6:]) # print jihyo
