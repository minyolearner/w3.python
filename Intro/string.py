# Python String

x = """ Multi
Line 
String 
in
Python""" #single qoute supported too



## Strings are Arrays
## Like many other popular programming languages, strings in Python are arrays of bytes representing unicode characters.

txt = 'I Love Jihyo'

print(txt[0]) # Print I

## Looping Through a String

for x in txt:
    print(x)
    

## String Length

print(len(txt))

## Check String

print('love' in txt) #case-sensitive .. it return false
print('Love' in txt) #case-sensitive .. it return true

if 'Love' in txt:
    print('Yes! You Fall in love for Jihyo')
    
if 'Mina' not in txt: #it return true
    print('No! You not Fall in love for Mina.. fact it test only.. really u love her too')
