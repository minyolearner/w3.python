# Data Type Practice

a = str(10)

print(type(a))

b = int('10')

print(type(b))

c = float(9)

print(type(c))

d = complex(1j)

print(type(d))

e = list(('Momo','Mina','Jihyo')) #mutable

print(type(e))

f = tuple(("Momo", "Mina", "Tyuzu")) # immutable(fixed)

print(type(f))

g = range(10)

print(g)
print(type(g))

h = dict(name = 'Momo',Citizen = 'japanese') #key cannot be 'here' also mutable

h['name'] = 'Mina' # off topic .. code runnder some time gives error


print(h)
print(type(h))

i = frozenset(('Tyuzu','Jihyo')) # unordered collection of unit item .. also immatuable

print(i)

print(type(i))

j = bool(5)

print(j)
print(type(j))

k = bytes(5)

print(k)
print(type(k))

l = bytearray(5)
print(l)
print(type(l))

m = memoryview(bytes(5))

print(m)
print(type(m))

print(memoryview(k))