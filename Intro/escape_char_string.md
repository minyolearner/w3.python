# Escape Character

To insert characters that are illegal in a string, use an escape character.



| \'   | Single Quote    | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_string_escape2)   |
| ------ | ----------------- | -------------------------------------------------------------------------------------------- |
| \\   | Backslash       | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_string_backslash) |
| \n   | New Line        | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_string_newline)   |
| \r   | Carriage Return | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_string_r)         |
| \t   | Tab             | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_string_t)         |
| \b   | Backspace       | [Try it »](https://www.w3schools.com/python/showpython.asp?filename=demo_string_b)        |
| \f   | Form Feed       |                                                                                            |
| \ooo | Octal value     | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_string_octal)     |
| \xhh | Hex value       |                                                                                            |
