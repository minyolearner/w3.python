# 

nameList = ['Momo', 'Jihyo', 'Tyuzu', 'Chengyoung','Jihyo']

print(nameList)

# List items are ordered, changeable, and allow duplicate values.
# List items are indexed, the first item has index [0], the second item has index [1] etc.

print(len(nameList)) #count duplicate too


# list() constructor
JihyoList = list((25 ,'South Korean', 'I Can\'t stop me', 'Twice', 'Lovie')) #Note double Round Brackets

print(JihyoList)