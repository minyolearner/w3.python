nameList = ['Momo', 'Jihyo']

# Add List item at end

nameList.append('Sana')
print(nameList)


# Insert Item on specific index without replacing that

nameList.insert(0, 'Nayeon')

print(nameList)


# Extend List

nameListextend = ['Tyuzu','Sana']

nameList.extend(nameListextend)

print(nameList)

# with extend() u can also use any iterabaly

twice = tuple(('Dahyun','Mina'))

nameList.extend(twice)

print(nameList)