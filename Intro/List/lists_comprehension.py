#List comprehension offers a shorter syntax when you want to create a new list based on the values of an existing list.

# Without Comprehension

nameList = ['Nayeon', 'Momo', 'Jihyo', 'Sana', 'Tyuzu', 'Sana', 'Dahyun', 'Mina']

newList = []

for x in nameList:
    newList.append(x)
    
print(newList)

nameListShort = []

for x in newList:
    if 'M' in x:
        nameListShort.append(x)
        
print(nameListShort)

# Loop Using Comrehension .. tldr short line

newAnotherList = [x for x in nameList]


print(newAnotherList)

list3 = [x for x in nameList if 'a' in x]

print(list3)



# @Need To Fix This
list4 = [x for x in nameList if 'a' or 'o' in x] 
print(list4)

list5 = [x for x in nameList if x not in 'Momo']

print(list5)

# Use iterator function to create list
list6 = [x for x in range(10)]

print(list6)

list7 = [x for x in range(10) if x <= 6]

print(list7)

# Use method in list comprehension

list8 = [x.upper() for x in nameList]

print(list8)

# Some Others Technique

list9 = ['I love Jihyo' for x in nameList]

print(list9)

# More Technieuq

list10 = ['I Love '+ x for x in nameList]

print(list10)

# Technique Pro Max

list11 = ['its the other condotion lol ' if x != 'Sana' else 'Orange' for x in nameList]

print(list11)

#Technique Pro Max Mini

list12 = [x if x != 'Sana' else 'Orange' for x in nameList]

print(list12)