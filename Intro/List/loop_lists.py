nameList = ['Nayeon', 'Momo', 'Jihyo', 'Sana', 'Tyuzu', 'Sana', 'Dahyun', 'Mina']

for x in nameList:
    print(x + ' is nice girl')
    


print('\n')

# Loop Through Index Number

for x in range(len(nameList)):
    print(nameList[x])


# Using While Loop
print('\n')

i =  0
while i <= 7:
    print(nameList[i])
    i = i + 1

# another way to write it 

i = 0

while i < len(nameList): # less than
    print(nameList[i])
    i += 1