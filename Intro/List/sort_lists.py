

nameList = ['Nayeon', 'Momo', 'Jihyo', 'Sana', 'Tyuzu', 'Sana', 'Dahyun', 'Mina']

# Sort List

nameList.sort()

print(nameList)

# reverse

nameList.sort(reverse=True)
# or
# nameList.reverse()

print(nameList)

# Customize Short Function Pro Max

# case sensitive sort

nameList = ['a','Nayeon', 'Momo', 'Jihyo', 'Sana', 'Tyuzu', 'Sana', 'Dahyun', 'Mina']

nameList.sort()

print(nameList)

# case insensitive

nameList.sort(key=str.lower)
print(nameList)
