#
nameList = ['Momo', 'Jihyo', 'Tyuzu', 'Chengyoung','Jihyo']


print(nameList[1])

nameList[3] = 'Nayeon'

print(nameList[3])
# Negative Index

print(nameList[-1]) #last item is negative index -1

print(nameList[-5]) #Negative Index Count From -1 ... 

# # Momo has 0 index .. also -5 index..

# Rannge Index Number

print(nameList[1:5]) #Last Item index is 4  

print(nameList[0:100]) #Last Item index is 4  ## worked!!

print(nameList[0:-1]) #Use Negative & Positive Index same time


print(nameList[:-2]) # Last 2 index not showing
print(nameList[1:]) #First 2 index not showing

# Check If Item Exixt

if 'Jihyo' in nameList:
    print('Yes, You Love Jihyo')