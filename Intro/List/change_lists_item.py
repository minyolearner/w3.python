nameList = ['Momo', 'Jihyo', 'Tyuzu','Sana','Nayeon']

# change list item . range
nameList[1:3] = ['Tyuzu','Jihyo']

print(nameList)


nameList[1:3] = ['Jihyo'] #it auto delete others

print(nameList)


nameList[1:2] = ['Jihyo','Mina','Tyuzu'] # auto add all

print(nameList)

# Insert a value in index with out losing other

nameList.insert(0, 'Twice')

print(nameList)