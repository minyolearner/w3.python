
nameList = ['Dahyun', 'Jihyo', 'Mina', 'Momo', 'Nayeon', 'Sana', 'Sana', 'Tyuzu']

# Shallow Copy & Deep Copy(Real Copy)

myList = nameList
myListReal = nameList.copy()


nameList[0] = 'Me'


print(myList) #Shallow Copy
print(myListReal) #Deep Copy