nameList = ['Nayeon', 'Momo', 'Jihyo', 'Sana', 'Tyuzu', 'Sana', 'Dahyun', 'Mina']


# Remove Specified Item

nameList.remove('Nayeon')

print(nameList)

# Remove Specific Index Number

nameList.pop(0) # pop() **if u not define index number it remove last item **  

print(nameList)

# It Also Remove Last Item
del nameList[1]

print(nameList)

# del also delete full list

del nameList

# print(nameList) it gives error.. cz itz delete ful list

# # Empty List clear()

nameList = ['Nayeon', 'Momo', 'Jihyo', 'Sana', 'Tyuzu', 'Sana', 'Dahyun', 'Mina']

nameList.clear()

print(nameList)