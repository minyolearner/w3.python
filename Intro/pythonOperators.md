# Python Operators

Python divides the operators in the following groups:

* Arithmetic operators
* Assignment operators
* Comparison operators
* Logical operators
* Identity operators
* Membership operators
* Bitwise operators

## Arithmatic Operators


| +  | Addition       | x + y  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_add)  |
| ---- | ---------------- | -------- | ------------------------------------------------------------------------------------- |
| -  | Subtraction    | x - y  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_sub)  |
| *  | Multiplication | x * y  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_mult) |
| /  | Division       | x / y  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_div)  |
| %  | Modulus        | x % y  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_mod)  |
| ** | Exponentiation | x ** y | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_exp)  |
| // | Floor division | x // y |                                                                                     |

## Comparison Operators


| == | Equal                    | x == y | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_compare1) |
| ---- | -------------------------- | -------- | ----------------------------------------------------------------------------------------- |
| != | Not equal                | x != y | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_compare2) |
| >  | Greater than             | x > y  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_compare4) |
| <  | Less than                | x < y  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_compare5) |
| >= | Greater than or equal to | x >= y | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_compare6) |
| <= | Less than or equal to    | x <= y | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_compare7) |

## Logical Operators


| and | Returns True if both statements are true                | x < 5 and  x < 10     | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_logical1) |
| ----- | --------------------------------------------------------- | ----------------------- | ----------------------------------------------------------------------------------------- |
| or  | Returns True if one of the statements is true           | x < 5 or x < 4        | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_logical2) |
| not | Reverse the result, returns False if the result is true | not(x < 5 and x < 10) |                                                                                         |

## Identity Operators


| is     | Returns True if both variables are the same object     | x is y     | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_identity1) |
| -------- | -------------------------------------------------------- | ------------ | ------------------------------------------------------------------------------------------ |
| is not | Returns True if both variables are not the same object | x is not y |                                                                                          |

## Membership Operators


| in     | Returns True if a sequence with the specified value is present in the object     | x in y     | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_membership1) |
| -------- | ---------------------------------------------------------------------------------- | ------------ | -------------------------------------------------------------------------------------------- |
| not in | Returns True if a sequence with the specified value is not present in the object | x not in y |                                                                                            |

## Python Bitwise Operators


| &  | AND                  | Sets each bit to 1 if both bits are 1                                                                   |
| ---- | ---------------------- | --------------------------------------------------------------------------------------------------------- |
|    |                      | OR                                                                                                      |
| ^  | XOR                  | Sets each bit to 1 if only one of two bits is 1                                                         |
| ~  | NOT                  | Inverts all the bits                                                                                    |
| << | Zero fill left shift | Shift left by pushing zeros in from the right and let the leftmost bits fall off                        |
| >> | Signed right shift   | Shift right by pushing copies of the leftmost bit in from the left, and let the rightmost bits fall off |


## Python Assignment Operators


| =   | x = 5   | x = 5      | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass1)  |
| ----- | --------- | ------------ | -------------------------------------------------------------------------------------- |
| +=  | x += 3  | x = x + 3  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass2)  |
| -=  | x -= 3  | x = x - 3  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass3)  |
| *=  | x *= 3  | x = x * 3  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass4)  |
| /=  | x /= 3  | x = x / 3  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass5)  |
| %=  | x %= 3  | x = x % 3  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass6)  |
| //= | x //= 3 | x = x // 3 | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass7)  |
| **= | x **= 3 | x = x ** 3 | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass8)  |
| &=  | x &= 3  | x = x & 3  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass9)  |
| ^=  | x ^= 3  | x = x ^ 3  | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass11) |
| >>= | x >>= 3 | x = x >> 3 | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass12) |
| <<= | x <<= 3 | x = x << 3 | [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass12) |


|=	x |= 3	x = x | 3 [Try it »](https://www.w3schools.com/python/trypython.asp?filename=demo_oper_ass10)