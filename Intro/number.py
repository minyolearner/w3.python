x = 1    # int

#convert from int to complex:
c = complex(x)

print(c)

# Note: You cannot convert complex numbers into another number type.

import random

print(random.random())
print(random.randrange(0,100))
print(random.choice((10,12,13)))
