# Single Line Comment

"""
Multi Line Comment
"""

print('Hello')

# variable Data type

a = str(10) # '10'
b = int('10') # 10
z = float(10) # 10.0

print(a , b , z)

# get type
print(type(a)) 
print(type(b))
print(type(z))

# Variable Naming
# CamelCase

myVarName = 'Jihyo'

#Pascal Case
MyVarName = 'Jihyo'

#Snake Case
my_var_name = 'Jihyo'


# Assign Multiple Values

p , q , r = 'momo' , 'jihyo' , 10

print(p, q, r)

#  same value on multiple variables

m = n = s = 'Tyuzu'

print(m, n, s)

# Unpack e collection (tupple)

twice = ['momo','nayeon','mina']

t , l , u = twice

print(t, l, u) 

# Print 

print('python' + ' awesome')
print('python' + ' awesome' , 17) # + cant use 

# Global Variable

def my_func():
    print(myVarName)
    twice_memeber = 9

my_func()

# print(twice_member)  // throw error cz its local

def my_func():
    global twice_member 
    twice_member = 9
    return 0
my_func()

print(twice_member)

# btw function can be recreated

