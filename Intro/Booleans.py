
x = 'jihyo'

y = 26

z = 0

print(bool(y))

print(bool(z)) #False


print(bool(["apple", "cherry", "banana"]))


# Following values are false
bool(False)
bool(None)
bool(0)
bool("")
bool(())
bool([])
bool({})

# Function & Class

def myfunc():
    return 0

print(bool(myfunc()))

class newClass():
    def __len__(self):
        return 0

myObject = newClass()

print(bool(myObject))

# Execute Function Based On Value

def jihyo():
    return True

if jihyo():
    print('I Love Jihyo')
else:
    print('I don\'t know')
    

# isinstance() function, which can be used to determine if an object is of a certain data type

Jihyo = 25

print(isinstance(Jihyo, int))