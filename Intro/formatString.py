



artist = 'Jihyo'
age = 25

secret = 'i love {},\n her age is {}'.capitalize()

print(secret.format(artist,age))

print(secret.format(artist,age).upper()) ## Multiple Method Same Time

## Format Sequence


secret = 'i love {1}.\n her age is {0}'

print(secret.format(age,artist))