txt = 'I love Jihyo'


### Remember Almost Everything is Object in python 
### So All Method(function) apply like this

print(txt.upper())

print(txt.lower())

# The strip() method removes any whitespace from the **beginning or the end**:
print(txt.strip())

#Replace String

print(txt.replace('Jihyo', 'Tyuzu'))

# Split String With Separator

print(txt.split(" ")) # it make this list
