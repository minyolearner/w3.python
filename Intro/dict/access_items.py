
about = {
    "Name": "Me",
    "Country": "South Korea",
    "City": "Seoul",
    "Citizen": "Korean",
    "Fav Number": 7,
}

print(type(about)) # Type Checking

# accessing an element

x = about['Name']

print(x)

# another way 

y = about.get("Name")

print(y)

# Get Keys
z = about.keys()

print(type(z))
print(z)

# Changin Item

about['Name'] = 'Jihyo'

print(about['Name'])


# Get values 
# alternative to Keys

m = about.values()
print(m)

# If Exist

if "Name" in about: #Key Only valid.. not value
    print("You Fall in Love Jihyo")