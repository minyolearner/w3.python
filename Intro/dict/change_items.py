

lover = {
    "Name": "Jihyo",
    "Country": "South Korea",
    "City": "Seoul",
    "Citizen": "Korean",
    "Fav Number": 7,
}

# Changing Value

lover['Name'] = 'Tyuzu'

print(lover['Name'])

# Another Way

lover.update({"Name":"Me"})

print(lover['Name'])