# Nested Dictionary

GirlFriend = {
    'Jihyo' : {
        'Age' : 26,
        'Country' : 'South Korea'
    },
    'Tyuzu' :{
        'Age': 27,
        'Country':'Taiwan'
    },
    'Dahyun':{
        'Age': 30,
        'Country': 'Japan'
    }
}


print(GirlFriend['Dahyun']['Country'])


# Another Example

child1 = {
  "name" : "Tyuzy",
  "year" : 1999
}
child2 = {
  "name" : "Jihyo",
  "year" : 1997
}
child3 = {
  "name" : "Mina",
  "year" : 1995
}

Family = {
  "child1" : child1,
  "child2" : child2,
  "child3" : child3
}