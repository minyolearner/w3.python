# As of Python version 3.7, dictionaries are ordered. In Python 3.6 and earlier, dictionaries are unordered.


mydict: dict = {
    "Name": "Me",
    "age" : 22
}

print(mydict["Name"])

mydict2 = dict(
    name = "me",
    age = 22
) #syntax quit deferent