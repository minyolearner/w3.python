# Intialize Dictionary

lover = {
    "Name": "Jihyo",
    "Country": "South Korea",
    "City": "Seoul",
    "Citizen": "Korean",
    "Fav Number": 7,
}

# Add New Item

lover['Band'] = 'Twice'
lover['is_kpop'] = True

print(lover)

# use update() method

lover.update({"Age":26})

print(lover)