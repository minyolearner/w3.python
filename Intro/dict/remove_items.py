# Intialize Item
lover = {
    "Name": "Jihyo",
    "Country": "South Korea",
    "City": "Seoul",
    "Citizen": "Korean",
    "Fav Number": 7,
}

# Remove Item
# The pop() method remove item with key

lover.pop("Citizen")

print(lover)

# popitem() method remove last item

lover.popitem()

print(lover)

# del keyword also use for delete item

del lover['City']

print(lover)

# it also delete full dict

# del lover

# print(lover) 


lover.clear()

print(lover)