# Intialize Item
lover = {
    "Name": "Jihyo",
    "Country": "South Korea",
    "City": "Seoul",
    "Citizen": "Korean",
    "Fav Number": 7,
}


# Loop Through Keys

for x in lover:
    print(x)
    
# Loop Through Values
for x in lover.values(): # normally loop key.. for value use values() method
    print(x)

# Another technique 
for x in lover:
    print(lover[x])
    
    
# Test
z = lover.items()

print(z)

# Loop Values & Key Both

for x, y in lover.items():
    print(x," = " ,y)