# Intialize Item
lover = {
    "Name": "Jihyo",
    "Country": "South Korea",
    "City": "Seoul",
    "Citizen": "Korean",
    "Fav Number": 7,
}


# Copy Dictionary
lover2 = lover.copy()
lover2['Name'] = 'Tyuzu'

print(lover2)


# Another Way to Copy

lover3 = dict(lover2)
lover3.update({'Name':'Jennie'})

print(lover3)
