# A lambda function is a small anonymous function.

# A lambda function can take any number of arguments, but can only have one expression.

# Syntax
# lambda paramter : expression

# Lamda Automitacally Return Values.. dont need return keyword

x = lambda param : 'return values'


print(x('a'))


y = lambda p : p + 1

print(y(1))



t = lambda p : p * 5

print(t(5))


r = lambda p,p2 : p * p2

print(r(5,7))


# Why Lamda Functions?
# The power of lambda is better shown when you use them as an anonymous function inside another function.
# Say you have a function definition that takes one argument, and that argument will be multiplied with an unknown number


# Just remember u can return lambda function on real function

def myfunc(n):
  return lambda a : a * n

mydoubler = myfunc(2)

print(mydoubler(11))

 
# Use lambda functions when an anonymous function is required for a short period of time.