# A function is a block of code which only runs when it is called.

# You can pass data, known as parameters, into a function.

# A function can return data as a result.

# Information can be passed into functions as arguments.

# Arguments are specified after the function name, inside the parentheses. 

# You can add as many arguments as you want, just separate them with a comma.


def printName(a):
    print(a)
    

print('Yeonwoo')
print('jihyoo')
print('Dahyun')

# Arbitrary Arguments, *args


def testOne(*a):
    print(type(a))
    print(a[0] + ' ' + a[1]+ ' '+a[2])


testOne('Yeonwoo','Jihyo','Dahyun')

# Arbitrary Arguments are often shortened to *args in Python documentations.


# Keyword Arguement

def loveK(name , name2 , name3):
    print('I love ' + name2)

loveK(name = 'Jihyo', name2 = 'Yeonwoo', name3 = 'Dahyun')
# The phrase Keyword Arguments are often shortened to kwargs in Python documentations.


# Arbitrary Keyword Arguments, **kwargs

# If you do not know how many keyword arguments that will be passed into your function,
# add two asterisk: ** before the parameter name in the function definition.

def loveT(**arg):
    print(f"i love {arg['name1']} i also love {arg['name2']}")
    
loveT(name1 = 'Jihyo' , name2 = 'Dahyun' ,name3 = 'Yeonwoo')

# Arbitrary Kword Arguments are often shortened to **kwargs in Python documentations.



# Defualt Parameter

def LoveN(love = 'Yeonwoo'):
    print(f"I love {love}")
    

LoveN()
LoveN('Jihyo')
LoveN(love = 'Dahyun') 


# Passing a List as an Argument

nameList = ['Yeonwoo','Tyuzu','Jihyo','Mina','Dahyun']

def loveU(param):
    for x in param:
        print(x)

loveU(nameList)

# Return Values

def loveReturn():
    return 'Jihyo'


print(loveReturn())

# Function Cannot Be Empty
# Function pass statement
# function definitions cannot be empty, but if you for some reason have a function definition with no content,
# put in the pass statement to avoid getting an error.

def LoveEmpty():
    pass

