''' 

The try block lets you test a block of code for errors.

The except block lets you handle the error.

The else block lets you execute code when there is no error.

The finally block lets you execute code, regardless of the result of the try- and except blocks.

'''

# Since the try block raises an error, the except block will be executed.


# Basic uses
try:
    print(x)
except:
    print('some error occured')
    

print('\n \n2nd example from here')

# Many Exceptions


try:
    print(x)
except NameError:
    print('var not define')
except:
    print('some error occured')


print('\n \n3rd example from here')

# Else Block
# You can use the else keyword to define a block of code to be executed if no errors were raised:

try:
    print('Seoul')
except:
    print('some error')
else:
    print('code is working properly')
    
    
print('\n \n4th example from here')

# Finally Block ..

# The finally block, if specified, 
# will be executed regardless if the try block raises an error or not.
try:
    print(x)
except:
    print('u got some error')
finally:
    print('if error occurd .. finally will work always')
    

# Make own error via raise keyword

'''

x = -1

if x < 0:
    raise Exception('Some Error.. program will stop here')

'''

# Another Example Pro Max




x = 'Seoul'

if not type(x) is int:
    raise TypeError('number expected.. you type an string.. program stopped')
    
    
