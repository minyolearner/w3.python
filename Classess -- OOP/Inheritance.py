# Python Inheritance

# Inheritance allows us to define a class that inherits all the methods and properties from another class.

# Parent class is the class being inherited from, also called base class.

# Child class is the class that inherits from another class, also called derived class.

class NameList:
    
    def __init__(self,listitem):
        self.allName = listitem

    def lLove(self):
        return 'This is from Base Class or Parent Class'



# Creating Object

# nameListObject = NameList('Jihyo')

class LikeList(NameList):
    pass


#Creating object from Like List

likelistObject = LikeList(['Jihyo','Yeonwoo','Momo','Dahyun'])

print(likelistObject.allName[0])


# Add Init Function
# Note: The child's __init__() function overrides the inheritance of the parent's __init__() function.
# When you add the __init__() function, the child class will no longer inherit the parent's __init__() function.
class ExtraList(NameList):
    
    def __init__(self,listitem): #parent __init__ now replaced with child __init__
        # To Keep Parent __init__ use this
        NameList.__init__(self, listitem)
    

object1 = ExtraList(['Jihyo','Yeonwoo','Momo','Dahyun'])

print(object1.allName[1])


# Super() Functions ?? Method
# Python also has a super() function that will make the 
# child class inherit all the methods and properties from its parent:

class newClass1(NameList):
    def __init__(self, listitem):
        super().__init__(listitem)
    # By using the super() function, 
    # you do not have to use the name of the parent element, 
    # it will automatically inherit the methods and properties from its parent.

object2 = newClass1(['Jihyo','Yeonwoo','Momo','Dahyun'])

print(object2.allName[0])

print(object2.lLove())


# Add Properties on init


class newClass2(NameList):
    def __init__(self,listitem,age):
        super().__init__(listitem)
        self.age = age
        

object3 = newClass2(['Jihyo','Yeonwoo','Momo','Dahyun'], 27)

print(object3.age)


# Add Method -- Same Way


# Just More

class newClass3(NameList):
    
    def __init__(self, *more ):
        NameList.__init__(self, more[0])
        self.age = more[1]

object4 = newClass3(['Jihyo','Yeonwoo','Momo','Dahyun'],27)

print(object4.allName)
print(object4.age)



# Pro Max
class newClass4(NameList):
    
    def __init__(self, **more ):
        NameList.__init__(self, more['name'])
        
        print('starting print \n \n ')
        
        for x in self.allName:
            print(x)
        
        print('\n \n \b end print')

object5 = newClass4(name = ['Jihyo','Yeonwoo','Momo','Dahyun'])



        