# Python is an object oriented programming language.

# Almost everything in Python is an object, with its properties and methods




# Create a Class

class MyClass:
    x = 5 #variable called properties


# Create an Object

o1 = MyClass()

# Print a properties from class using created object

print(o1.x)


# The __init__() Function
# All classes have a function called __init__(), which is always executed when the class is being initiated.
#  The __init__() function is called automatically every time the class is being used to create a new object.


class MyClass2:
    def __init__(self):
        print('a')
        

object2 = MyClass2()


# Example 2 __init__(self)


class MyClass3():

    def __init__(self,name,age,txt): #init 
        self.her_name = name
        self.her_age = age
        self.my_txt = txt
    
    # myGf = her_name #init properties out of scope


object3 = MyClass3('Jihyo', 27,'I love Her')

print(object3.her_name)
print(object3.her_age)
print(object3.my_txt)


# The __str__() Function
# The __str__() function controls what should be returned when the class object is represented as a string.


class MyClass4:
    
    def __init__(self,name,age):
        self.her_name = name
        self.her_age =  age
    
    def __str__(self):
        return (f"Her name is {self.her_name} & age is {self.her_age}") #It must be return.. no print
        

object4 = MyClass4('Jihyo', 27)


print(object4)


# Object Method ?? Class Method

class MyClass5:

    def itsMethod(self): #must need self !lol
        return 'I love Yeonwoo'
    
# Note: The self parameter is a reference to the current instance of the class, 
# and is used to access variables that belong to the class.

object5 = MyClass5()


print(object5.itsMethod())


# The self Parameter
# It does not have to be named self , you can call it whatever you like,
# but it has to be the first parameter of any function in the class:
class MyClass6:
    # Properties
    name = 'Dahyun'

    bigname = 'Mina' # init cannot replace main class propertis

    
    
    def __init__(self,name):
        self.name = name
        
        
    # My Method
    # The self parameter is a reference to the current instance of the class,
    # and is used to access variables that belongs to the class.
    def its_method(self): #It does not have to be named self , But need to be first parameter
        return self.name # init properties replace main properties
    

# Creating Object
 
object6 = MyClass6('Jihyo')

print(object6.its_method())
print(object6.bigname)


# Modify Object Properties
# only edit self properties not main properties

object6.name = 'Yeonwoo'

print(object6.name)

# Delete Object Properties
# only del self properties not main properties

del object6.name

print(object6.name) #print main value.. dahyun..


# Delete Object

del object6

# The pass Statement
# Class cannot be empty

class MyClass7:
    pass
