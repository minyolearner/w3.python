# Built in module
 
import platform as pt 

print(pt.system())

# Using the dir() Function
# There is a built-in function to list all the function names (or variable names) in a module. The dir() function:

print(dir(pt))