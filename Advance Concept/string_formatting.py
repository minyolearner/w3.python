# To make sure a string will display as expected, we can format the result with the format() method.


# Basic uses
birthYear = 1995
txt = 'her birth year {}'

print(txt.format(birthYear))



# Formating 
price = 49
txt2 = 'item price is {:.2f} currencyName'

print(txt2.format(price))

# Strign Format Reference
# https://www.w3schools.com/python/ref_string_format.asp


# Multiple uses

a = 20
b = 30
c = 'Momo'

txt3 = 'some {} some keyword then {} .. more keyword then {}'

print(txt3.format(a,b,c))


# Indexing using number for multiple uses

a = 20
b = 30
c = 'Momo'

txt3 = 'some {0} some keyword then {2} .. more keyword then {1}'

print(txt3.format(a,b,c))



# Named Index


txt4 = 'some text for {name}'

print(txt4.format(name = 'Momo'))