# Scope in Python

# A variable is only available from inside the region it is created. This is called scope.


#  Local Scope


def myfunc():
    love = 'jihyo'
    
myfunc()

# print(love) not defined


# making local scope to global


def myfunc1():
    global love
    love = 'Yeonwoo'
    
myfunc1()

print(love) 