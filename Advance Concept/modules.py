# # Module

# Consider a module to be the same as a code library.

# A file containing a set of functions you want to include in your application.


import mod_function


mod_function.myLove('Jihyo')

mod_function.myLove('Yeonwoo')


x = mod_function.FavSong(['Yeonwoo','Jiso','Jihyo','Dahyun','Mina','Tyuzu'])

print(x.name[3])


mod_function.Cute.mytime() # method called from module class