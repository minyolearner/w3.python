# Iterators
'''
An iterator is an object that contains a countable number of values.

An iterator is an object that can be iterated upon, meaning that you can traverse through all the values.

Technically, in Python, an iterator is an object which implements the iterator protocol,

which consist of the methods __iter__() and __next__().

'''


# Basic Iterator


# list intialize
nameList = ['Yeonwoo','Jiso','Jihyo','Dahyun','Mina','Tyuzu']

myit = iter(nameList)

print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))


# String Also Iterable

myDreamGirl = 'Yeonwoo'

myit1 = iter(myDreamGirl)

print(next(myit1))
print(next(myit1))
print(next(myit1))


# Iterator Advance -- Creating Iterator

'''
To create an object/class as an iterator you have to implement the methods __iter__() and __next__() to your object.

As you have learned in the Python Classes/Objects chapter, all classes have a function called __init__(), which allows you to do some initializing when the object is being created.

The __iter__() method acts similar, you can do operations (initializing etc.), but must always return the iterator object itself.

The __next__() method also allows you to do operations, and must return the next item in the sequence.

'''

# BTW I dont find any useful for building own iterator __iter__ __next__
class myClass:
    
    def __iter__(self):
        self.a = 1
        return self #must return self
    
    
    
    def __next__(self): #what happend when next called :) 
        # dorabada niyom nai.. ja ecca dewa jabe..  
        
        # akta new number add hobe.. then abar return korbe..  
        x = self.a

        self.a += 1
        
        return x


myClassObject = myClass()

myit2 = iter(myClassObject)

print(next(myit2))
print(next(myit2))
print(next(myit2))


# Limit Iterator raise StopIteration

print('\n \n \n \n')

class myClass1:
    def __iter__(self):
        self.a = 1
        return self
    
    
    
    def __next__(self):
        
        if self.a <= 10:
            x = self.a
            self.a += 1
            return x
        # fixed kore dilam ki ar korar.. naile sobay huday dayka jamla korbe
        else:
            raise StopIteration
        
myClassObject1 = myClass1()
myit3 = iter(myClassObject1)

print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))
print(next(myit3))


