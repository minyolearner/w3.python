# A date in Python is not a data type of its own, 
# but we can import a module named datetime to work with dates as date objects


import datetime

x = datetime.datetime.now()


print(x)

print(x.strftime("%B")) #printing Month name

print(type(x))


y = datetime.datetime(2018,6,1).strftime("%B")
z = datetime.datetime(2018,6,1).strftime("%b")

print(y)
print(z)

# More at 
# https://www.w3schools.com/python/python_datetime.asp