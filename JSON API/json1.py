# JSON is a syntax for storing and exchanging data.

# JSON is text, written with JavaScript object notation.


import json

x =  '{ "name":"Jihyo", "age":27, "city":"South Korean"}' 

# parse json to python

y = json.loads(x) #it will be dict

print(type(y))
print(y)


# python to json

nameList = {'Name':'Yeonwoo','Citizen':'South Korean' , 'age':28}


z = json.dumps(nameList)

print(type(z)) #it look like string
print(z)

# Formating JSON

z = json.dumps(nameList, indent= 4) # make it easier to read

print(z)


# Using Seprator

t = json.dumps(nameList, indent= 4, separators=('. ','='))

print(t)


# Order Result

n = json.dumps(nameList,indent=4,sort_keys=True)

print(n)