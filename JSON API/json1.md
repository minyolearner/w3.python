# Python To JSON



When you convert from Python to JSON, Python objects are converted into the JSON (JavaScript) equivalent:


| Python | JSON   |
| -------- | -------- |
| dict   | Object |
| list   | Array  |
| tuple  | Array  |
| str    | String |
| int    | Number |
| float  | Number |
| True   | true   |
| False  | false  |
| None   | null   |
