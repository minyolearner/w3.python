# A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).


# This is less like the for keyword in other programming languages, and works more like an iterator method as found in other object-orientated programming languages.

# With the for loop we can execute a set of statements, once for each item in a list, tuple, set etc.

# Loop Throgh List dictionary and tuple , set
nameList = ['Yeonwoo','Tyuzu','Jihyo','Mina','Dahyun']

for x in nameList:
    print('i love ' + x)
    
    
# Loop Throough String

name = 'Momoland'

for x in name:
    print(x)


# it also has break

for x in nameList:
    if x == 'Mina':
        break 
    print('i love ' + x)
    
    
# use continue

for x in nameList:
    if x == 'Mina':
        continue
    print('i love ' + x)
    
    
# range funtion

for x in range(2,30,3 ):# start , end , step
    print(x)

# else

# The else keyword in a for loop specifies a block of code to be executed when the loop is finished:

for x in range(10):
    print('I love Yeonwoo')
else:
    print('Loop Finished')
    


# Nested For loop

for x in range(10):
    for y in nameList:
        print(x,y)

# Pass statement
# for loops cannot be empty, but if you for some reason have a for loop with no content,
# put in the pass statement to avoid getting an error.

for x in ['jihyo','mina','dahyun']:
    pass