# While Loop Intiliazition

i = 1
while i < 10:
    print('I love Yeonwoo')
    i += 1
# Recheck i = 10

print(i)

# Nested Loop and Break

a = 1

while a < 10:
    print(f"Number {a}")
    if a == 7:
        print("Stop Code")
        break
    a += 1
    
    
# Nested Loop and Continue

b = 1

while b <= 20:
    print(f"number is {b}")
    b += 1
    
    if b == 10:
        continue
    

# While also else

d = 1 

while d > 10:
    a += 1
    print(f"number is {a}")
else:
    print('I love Yeonwoo')