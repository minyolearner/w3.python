
# Variable intialize
a = 20
b = 22

# if elif else statement
if a > b:
    print('I hate jihyo')
    
    
if a > b:
    print('Whatever')
else:
    print('I love jihyo')


if a > b:
    print('whatever')
elif b > a:
    print('I love tyuzu')
else:
    print('Advance Concept/')
    
# short hand if 

if a > b : print('anything')

# Short Hand If Else

print('A') if b > a else print('B')

# if statements cannot be empty, but if you for some reason
# have an if statement with no content, 
# put in the pass statement to avoid getting an error.

if True:
    pass

